import * as express from "express";

var app = express();

app.use(express.static('../htdocs'));

var port = process.env.port || 8080;

app.listen(port, function () {
  console.log('Cubitt-static started file serving on port ' + port + '!');
})
